package Alien::LibCIAORegion;

# ABSTRACT: Find or build the CIAO Region library

use strict;
use warnings;

use base qw( Alien::Base );

our $VERSION = '0.02';

1;

# COPYRIGHT

__END__

=for stopwords
CXC
GPL

=head1 SYNOPSIS

  use Alien::LibCIAORegion;

=head1 DESCRIPTION

This module finds or builds the I<region> library extracted from
the Chandra Interactive Analysis of Observations (CIAO) software
package produced by the Chandra X-Ray Center (CXC).
See L<https://cxc.harvard.edu/ciao/> for more information.

Unfortunately, there is no documentation accompanying the library.

The region library is itself released under the GPL, version 3.  See
the enclosed copyright information.

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on L<metacpan|https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod>).

=head1 SEE ALSO

CIAO::Lib::Region
